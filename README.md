# Overview
This is an application with basic structure for image navigating and processing on Android platform.
# Guidance for running and testing
## Step 1
Import this project into your android studio (by opening this existing project in the welcome page of android studio)
## Step 2
Click on AVD Manager and add a virtual android device (e.g. Nexus 5 API 24)
IF you wanna perform test on your android phone, connect your phone to laptop, and copy (your project directory)/app/build/outputs/apk/app-debug.apk to your phone, then install and run it
## Step 3
Run your project in Android Studio with selected emulator  
## Step 4
Find the app with name IMGPROCS in your android device and run it  
## Step 5
Click on button Load, choose wav format files within audio files on your phone. If you are testing with emulator in Android Studio, you will have to manage copying your wav format files into emulator sd card, or use recorder on emulator to make some wav files for testing.
## Step 6
You can then click the button for grayscale to process selected wav format files.
## Step 7
Resulting Graphs along with their names will be displayed in grids (if the files to save are with great quality or great dimension, the app can be quite slow and may fail to display you pics or display with errors)
## Step 8
Click on button Save, and the resulting graphs will be saved in either cache directory of this application (check internal storage, and find these pictures in Android/data/ui.imgprocs/cache/WAVPROCS/) on you android phone or Photos/Pictures on emulator.
## Step 9
Please give us feedback for errors, exceptions and improvements!  