package ui.imgprocs;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Checkable;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Activity for page showing gray pics and save button
public class SaveActivity extends AppCompatActivity {

    private List<Uri> wavUris;
    private List<String> wavNames;
    private List<Bitmap> wavGraphs;
    private GridView wavTextGrid;
    private Button btnSave;
    private Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);

        Bundle bundle = this.getIntent().getExtras();
        wavUris = (List<Uri>) bundle.getSerializable("wavUris");
        wavNames = (List<String>) bundle.getSerializable("wavNames");

        wavGraphs = new ArrayList<>();//list containing resulting graphs after your processing of wav files
        for(Uri uri: wavUris){
            wavGraphs.add(imageGrey(uri));
        }

        wavTextGrid = (GridView) findViewById(R.id.wavTextGrid);//Gridview showing resulting graphs
        List<HashMap<String, Object>> wavTextList = new ArrayList<>();
        for(int index = 0; index < wavGraphs.size(); index++){
            HashMap<String, Object> map = new HashMap<>();
            map.put("itempic", wavGraphs.get(index));
            map.put("itemtext", wavNames.get(index));
            wavTextList.add(map);
        }
        final PicTextAdapter picTextAdapter = new PicTextAdapter(this,
                wavTextList,
                R.layout.item_pictext,
                new String[]{"itempic", "itemtext"},
                new int[]{R.id.itempic, R.id.itemtext});//customized adapter for Gridview to correctly show pics and picnames
        wavTextGrid.setAdapter(picTextAdapter);

        btnSave = (Button) findViewById(R.id.btnSave);
        View.OnClickListener oclBtnSave = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int index = 0; index < wavGraphs.size(); index++){//save bitmaps in cache dir of this app
                    File wavDir = new File(SaveActivity.this.getExternalCacheDir(), "WAVPROCS");
                    if(!wavDir.exists()){
                        wavDir.mkdirs();
                    }
                    File wavGraph = new File(wavDir, wavNames.get(index) + ".jpg");
                    if(wavGraph.exists()){
                        wavGraph.delete();
                    }
                    try {
                        wavGraph.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        FileOutputStream fos = new FileOutputStream(wavGraph);
                        wavGraphs.get(index).compress(Bitmap.CompressFormat.JPEG, 100, fos);//save as jpeg for max quality
                        fos.flush();
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //insert bitmap into system gallery of your android device
                    MediaStore.Images.Media.insertImage(SaveActivity.this.getContentResolver(), wavGraphs.get(index), wavNames.get(index), wavUris.get(index).getPath());
                }
            }
        };
        btnSave.setOnClickListener(oclBtnSave);

        btnBack = (Button) findViewById(R.id.btnBack2);
        View.OnClickListener oclBtnBack = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveActivity.this.finish();
            }
        };//back button
        btnBack.setOnClickListener(oclBtnBack);
    }

    //Todo: sample function here. you can add you processing of wav file here and return the resulting graph as a Bitmap
    private Bitmap imageGrey(Uri uri){

        Rect rect = new Rect(0, 0, 160, 120);
        Bitmap greybmp = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);//create new empty bitmap
        Canvas canvas = new Canvas(greybmp);
        Paint paint = new Paint();
        int color = Color.argb(255, 255, 245, 238);
        paint.setColor(color);
        canvas.drawRect(rect, paint);
        return greybmp;
    }
}

class PicTextAdapter extends BaseAdapter{//data adapter for Gridview

    private final LayoutInflater mInflater;
    private int[] mTo;
    private String[] mFrom;
    private List<? extends Map<String, ?>> mData;
    private int mResource;

    public PicTextAdapter(Context context, List<? extends Map<String, ?>> data,
                          @LayoutRes int resource, String[] from, @IdRes int[] to){
        mData = data;
        mResource = resource;
        mFrom = from;
        mTo = to;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView == null) {
            v = mInflater.inflate(mResource, parent, false);
        } else {
            v = convertView;
        }

        //bind Imageview(pic) and Textview(picname) into each grid in Gridview
        bindView(position, v);

        return v;
    }

    private void bindView(int position, View view) {
        final Map dataSet = mData.get(position);
        if (dataSet == null) {
            return;
        }

        final String[] from = mFrom;
        final int[] to = mTo;
        final int count = to.length;

        for (int i = 0; i < count; i++) {
            final View v = view.findViewById(to[i]);
            if (v != null) {
                final Object data = dataSet.get(from[i]);
                String text = data == null ? "" : data.toString();
                if (text == null) {
                    text = "";
                }

                if (v instanceof TextView) {
                    // Note: keep the instanceof TextView check at the bottom of these
                    // ifs since a lot of views are TextViews (e.g. CheckBoxes).
                    ((TextView) v).setText(text);
                } else if (v instanceof ImageView) {
                    if (data instanceof Integer) {
                        ((ImageView) v).setImageResource ((Integer) data);
                    } else if (data instanceof Bitmap){//load bitmap into Imageview
                        ((ImageView) v).setImageBitmap((Bitmap) data);
                    } else {
                        try {
                            ((ImageView)v).setImageResource(Integer.parseInt((String) data));
                        } catch (NumberFormatException nfe) {
                            ((ImageView)v).setImageURI(Uri.parse((String) data));//load pic from Uri into ImageView
                        }
                    }
                } else {
                    throw new IllegalStateException(v.getClass().getName() + " is not a " +
                            " view that can be bounds by this PicTextAdapter");
                }
            }
        }
    }
}
