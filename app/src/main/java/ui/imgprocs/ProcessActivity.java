package ui.imgprocs;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//Activity for page displaying names of selected wavs
public class ProcessActivity extends AppCompatActivity {

    private List<Uri> wavUris;
    private List<String> wavNameList;
    private TextView wavNames;
    private Button btnGrey;
    private Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process);

        Bundle bundle = this.getIntent().getExtras();
        wavUris = (List<Uri>) bundle.getSerializable("wavUris");//get list of Uri of chose wavs from bundle passed to this Activity
        wavNames = (TextView) findViewById(R.id.wavNames);
        wavNameList = new ArrayList<String>();
        StringBuilder builder = new StringBuilder();
        builder.append("Selected WAVs:");
        for(Uri uri: wavUris){//Todo: get file name of each wav and show them in Textview. since these wav files are chosen from audio/ folder of android system, each wav file has been renamed by android system, and we cannot get the original name of each wav file for now.
            List<String> pathSegments = uri.getPathSegments();
            String string = pathSegments.get(pathSegments.size() - 1);
            wavNameList.add(string);
            builder.append("\n").append(string);
        }
        wavNames.setText(builder.toString());

        btnGrey = (Button) findViewById(R.id.btnGrey);//button for operation of turing pics to black & white (grayscale)
        View.OnClickListener oclBtnGrey = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("wavUris", (Serializable) wavUris);
                bundle.putSerializable("wavNames", (Serializable) wavNameList);
                Intent saveIntent = new Intent(ProcessActivity.this, SaveActivity.class);//pass uri list and name list to next Activity for display
                saveIntent.putExtras(bundle);

                startActivity(saveIntent);
            }
        };
        btnGrey.setOnClickListener(oclBtnGrey);

        btnBack = (Button) findViewById(R.id.btnBack);
        View.OnClickListener oclBtnBack = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProcessActivity.this.finish();//operation for back button
            }
        };
        btnBack.setOnClickListener(oclBtnBack);
    }
}
