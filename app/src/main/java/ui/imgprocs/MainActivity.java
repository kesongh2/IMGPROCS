package ui.imgprocs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Process;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//Activity for entry page
public class MainActivity extends AppCompatActivity {

    private static final int REQ_CHOOSE_WAVS = 1;
    TextView textView;
    Button btnLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //find view-elements
        textView = (TextView) findViewById(R.id.textView);
        btnLoad = (Button) findViewById(R.id.load);

        View.OnClickListener oclBtnLoad = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseWAVs();
            }
        };
        btnLoad.setOnClickListener(oclBtnLoad);
    }

    //choose WAV format file from storage using Android Intent Mechanism
    private void chooseWAVs(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("audio/*");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        }
        startActivityForResult(intent, REQ_CHOOSE_WAVS);

        //guidance message for choosing multiple files
        Toast.makeText(this, "single-selection: tap on any wav.\n" +
        "multi-selection: tap & hold on the first wav, " +
        "then tap for more, finally tap on OPEN to finish.",
                Toast.LENGTH_LONG).show();
    }

    @Override
    //listener function for open wav intent
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData){
        if(requestCode == REQ_CHOOSE_WAVS && resultCode == RESULT_OK){
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                ClipData clipData = resultData.getClipData();//get multiple wavs chose
                Bundle bundle = new Bundle();
                List<Uri> wavUris = new ArrayList<Uri>();
                if(clipData == null){//only one wav is chosen
                    wavUris.add(resultData.getData());
                }else {
                    for(int i = 0; i < clipData.getItemCount(); i++){
                        ClipData.Item item = clipData.getItemAt(i);
                        wavUris.add(item.getUri());//add Uri of each chose wav into list
                    }
                }
                for(Uri uri: wavUris){
                    if(!isWAV(uri)){
                        Toast.makeText(this, "Please select only wav format files!",
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                //pass uris of wav format files to next activity for later processing
                bundle.putSerializable("wavUris", (Serializable) wavUris);//put serialized list into bundle
                Intent processIntent = new Intent(this, ProcessActivity.class);//open next page
                processIntent.putExtras(bundle);//pass bundle

                startActivity(processIntent);
            }
        }
    }

    //judge if the file represented by given uri is of WAV format
    private boolean isWAV(Uri uri){
        try {
            //get inputstream of given uri to read bytes from wav format file. similar approach can be used later
            InputStream is = getApplicationContext().getContentResolver().openInputStream(uri);
            byte[] bytes = new byte[4];
            is.read(bytes, 0, 4);
            is.read(bytes, 0, 4);
            StringBuilder builder = new StringBuilder();
            builder.append((char)is.read()).append((char)is.read()).append((char)is.read()).append((char)is.read());
            if(!builder.toString().equals("WAVE")){//check the Format field in WAV format file header
                return false;
            }
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
